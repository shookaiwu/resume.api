﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;


namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class CertificationsController : Controller
    {
        private ILogger<CertificationsController> _logger;
        private IResumeRepository _resumeRepository;

        public CertificationsController(ILogger<CertificationsController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpGet("{personId}/certifications")]
        public IActionResult GetCertifications(int personId)
        {
            try
            {
                if (!_resumeRepository.GetCertificationsById(personId).Any())
                {
                    _logger.LogInformation($"Certifications with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var certificationsForPerson = _resumeRepository.GetCertificationsById(personId);

                var certificationsForPersonResults = Mapper.Map<IEnumerable<CertificationDto>>(certificationsForPerson);

                return Ok(certificationsForPersonResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting Certifications for person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{personId}/certification/{certificationId}", Name = "GetPersonCertification")]
        public IActionResult GetPersonCertification(int personId, int certificationId)
        {
            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var certification = _resumeRepository.GetCertificationById(personId, certificationId);

            if (certification == null)
            {
                return NotFound();
            }

            var certificationResult = Mapper.Map<CertificationDto>(certification);

            return Ok(certificationResult);
        }

        [HttpPost("{personId}/certification")]
        public IActionResult CreateCertification(int personId,
            [FromBody] CertificationForCreationDto certification)
        {
            if (certification == null)
            {
                return BadRequest();
            }

            if (certification.CertificationName == certification.CertificationAuthority)
            {
                ModelState.AddModelError("certification name", "The provided certification name should be different from the authority name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var finalCertification = Mapper.Map<Entities.Certification>(certification);

            _resumeRepository.AddCertification(finalCertification, personId);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdCertificationToReturn = Mapper.Map<Models.CertificationForCreationDto>(finalCertification);

            return CreatedAtRoute("GetPersonCertification", new
            { personId = personId, certificationId = finalCertification.CertificationId }, createdCertificationToReturn);
        }

        [HttpPut("{personId}/certification/{certificationId}")]
        public IActionResult UpdateCertification(int personId, int certificationId,
                [FromBody] CertificationForUpdateDto certification)
        {
            if (certification == null)
            {
                return BadRequest();
            }

            if (certification.CertificationName == certification.CertificationAuthority)
            {
                ModelState.AddModelError("Description", "The provided description should be different from the name.");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var certificationEntity = _resumeRepository.GetCertificationById(personId, certificationId);
            if (certificationEntity == null)
            {
                return NotFound();
            }

            Mapper.Map(certification, certificationEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpPatch("{personId}/certification/{certificationId}")]
        public IActionResult PartiallyUpdateCertification(int personId, int certificationId,
            [FromBody] JsonPatchDocument<CertificationForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var certificationEntity = _resumeRepository.GetCertificationById(personId, certificationId);

            if (certificationEntity == null)
            {
                return NotFound();
            }
            var certificationToPatch = Mapper.Map<CertificationForUpdateDto>(certificationEntity);

            patchDoc.ApplyTo(certificationToPatch, ModelState);

            TryValidateModel(certificationToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(certificationToPatch, certificationEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpDelete("{personId}/certification/{id}")]
        public IActionResult DeleteCertification(int personId, int certificationId)
        {

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var certificationEntity = _resumeRepository.GetCertificationById(personId, certificationId);
            if (certificationEntity == null)
            {
                return NotFound();
            }

            _resumeRepository.DeleteCertification(certificationEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

    }
}
