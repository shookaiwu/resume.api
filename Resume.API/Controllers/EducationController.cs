﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;


namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class EducationController : Controller
    {
        private ILogger<EducationController> _logger;
        private IResumeRepository _resumeRepository;

        public EducationController(ILogger<EducationController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }


        [HttpGet("{personId}/Education")]
        public IActionResult GetEducation(int personId)
        {
            try
            {
                if (!_resumeRepository.GetEducationsById(personId).Any())
                {
                    _logger.LogInformation($"Certifications with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var educationForPerson = _resumeRepository.GetEducationsById(personId);

                var educationForPersonResults = Mapper.Map<IEnumerable<CertificationDto>>(educationForPerson);

                return Ok(educationForPersonResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting Education for person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{personId}/education/{educationId}", Name = "GetPersonEducation")]
        public IActionResult GetPersonEducation(int personId, int educationId)
        {
            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var education = _resumeRepository.GetEducationById(personId, educationId);

            if (education == null)
            {
                return NotFound();
            }

            var educationResult = Mapper.Map<EducationDto>(education);

            return Ok(educationResult);
        }

        [HttpPut("{personId}/education/educationId"), HttpPost("{personId}/education")]
        public IActionResult AddOrUpdateEducation(int personId,
            [FromBody] EducationDto education)
        {
            if (education == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var finalEducation = Mapper.Map<Entities.Education>(education);

            _resumeRepository.AddOrUpdateEducation(finalEducation, personId);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdEducationToReturn = Mapper.Map<Models.EducationDto>(finalEducation);

            return CreatedAtRoute("GetPersonEducation", new
            { personId = personId, educationId = finalEducation.EducationId }, createdEducationToReturn);
        }

    }
}