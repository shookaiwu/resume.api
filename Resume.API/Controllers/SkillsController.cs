﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;

namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class SkillsController : Controller
    {
        private ILogger<SkillsController> _logger;
        private IResumeRepository _resumeRepository;

        public SkillsController(ILogger<SkillsController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpPost("{personId}/skill")]
        public IActionResult CreateSkill(int personId,
            [FromBody] SkillForCreationDto skill)
        {
            if (skill == null)
            {
                return BadRequest();
            }

            //if (skill.CertificationName == skill.CertificationAuthority)
            //{
            //    ModelState.AddModelError("certification name", "The provided certification name should be different from the authority name.");
            //}

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var finalSkill = Mapper.Map<Entities.Skill>(skill);

            _resumeRepository.AddSkill(finalSkill, personId);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdSkillToReturn = Mapper.Map<Models.SkillDto>(finalSkill);

            return CreatedAtRoute("GetPersonSkill", new
            { personId = personId, skillId = finalSkill.SkillId }, createdSkillToReturn);
        }

        [HttpGet("{personId}/skills")]
        public IActionResult GetSkills(int personId)
        {
            try
            {
                if (!_resumeRepository.GetSkillsById(personId).Any())
                {
                    _logger.LogInformation($"Skills with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var skillsForPerson = _resumeRepository.GetSkillsById(personId);

                var skillsForPersonResults = Mapper.Map<IEnumerable<SkillDto>>(skillsForPerson);

                return Ok(skillsForPersonResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting Skills for person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{personId}/skills/{skillId}", Name = "GetPersonSkill")]
        public IActionResult GetPersonSkill(int personId, int skillId)
        {
            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var skill = _resumeRepository.GetSkillById(personId, skillId);

            if (skill == null)
            {
                return NotFound();
            }

            var skillResult = Mapper.Map<SkillDto>(skill);

            return Ok(skillResult);
        }
    }
}