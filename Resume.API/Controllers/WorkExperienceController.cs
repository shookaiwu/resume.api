﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;
namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class WorkExperienceController : Controller
    {
        private ILogger<WorkExperienceController> _logger;
        private IResumeRepository _resumeRepository;

        public WorkExperienceController(ILogger<WorkExperienceController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpPost("{personId}/workexperience")]
        public IActionResult CreateWorkExperience(int personId,
            [FromBody] WorkExperienceForCreationDto workExperience)
        {
            if (workExperience == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var finalWorkExperience = Mapper.Map<Entities.WorkExperience>(workExperience);

            _resumeRepository.AddOrUpdateExperience(finalWorkExperience, personId);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdWorkExperienceToReturn = Mapper.Map<Models.WorkExperienceDto>(finalWorkExperience);

            return CreatedAtRoute("GetPersonWorkExperience", new
            { personId = personId, workExperienceId = finalWorkExperience.WorkExperienceId }, createdWorkExperienceToReturn);
        }

        [HttpGet("{personId}/workexperience")]
        public IActionResult GetWorkExperience(int personId)
        {
            try
            {
                if (!_resumeRepository.GetWorkExperienceById(personId).Any())
                {
                    _logger.LogInformation($"Skills with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var WorkExperienceForPerson = _resumeRepository.GetWorkExperienceById(personId);

                var WorkExperienceForPersonResults = Mapper.Map<IEnumerable<WorkExperienceDto>>(WorkExperienceForPerson);

                return Ok(WorkExperienceForPersonResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting Work Experience for person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{personId}/workexperience/{workExperienceId}", Name = "GetPersonWorkexperience")]
        public IActionResult GetPersonSkill(int personId, int workexperienceId)
        {
            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var workexperience = _resumeRepository.GetWorkExperienceById(personId, workexperienceId);

            if (workexperience == null)
            {
                return NotFound();
            }

            var workexperienceResult = Mapper.Map<WorkExperienceDto>(workexperience);

            return Ok(workexperienceResult);
        }
    }
}