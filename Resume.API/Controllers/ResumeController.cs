﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;

namespace Resume.API.Controllers
{
    [Route("api/resume")]
    public class ResumeController : Controller
    {
        private ILogger<ResumeController> _logger;
        private IResumeRepository _resumeRepository;

        public ResumeController(ILogger<ResumeController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpGet()]
        public IActionResult GetResume()
        {
            var resumeEntities = _resumeRepository.GetAllResumes();
            var results = Mapper.Map<IEnumerable<PersonDto>>(resumeEntities);

            return Ok(results);
        }

    }
}
