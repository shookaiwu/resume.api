﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;


namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class LanguagesController : Controller
    {
        private ILogger<LanguagesController> _logger;
        private IResumeRepository _resumeRepository;

        public LanguagesController(ILogger<LanguagesController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpPost("{personId}/languages")]
        public IActionResult CreateLanguage(int personId,
            [FromBody] LanguageForCreationDto language)
        {
            if (language == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var finalLanguage = Mapper.Map<Entities.Language>(language);

            _resumeRepository.AddLanguage(finalLanguage, personId);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdLanguageToReturn = Mapper.Map<Models.LanguageDto>(finalLanguage);

            return CreatedAtRoute("GetPersonLanguage", new
            { personId = personId, languageId = finalLanguage.LanguageId }, createdLanguageToReturn);
        }

        [HttpGet("{personId}/languages")]
        public IActionResult GetLanguages(int personId)
        {
            try
            {
                if (!_resumeRepository.GetLanguagesById(personId).Any())
                {
                    _logger.LogInformation($"Languages with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var languagesForPerson = _resumeRepository.GetLanguagesById(personId);

                var languagesForPersonResults = Mapper.Map<IEnumerable<LanguageDto>>(languagesForPerson);

                return Ok(languagesForPersonResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting Languages for person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpGet("{personId}/languages/{languageId}", Name = "GetPersonLanguage")]
        public IActionResult GetPersonLanguage(int personId, int languageId)
        {
            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var language = _resumeRepository.GetSkillById(personId, languageId);

            if (language == null)
            {
                return NotFound();
            }

            var languageResult = Mapper.Map<LanguageDto>(language);

            return Ok(languageResult);
        }
    }
}