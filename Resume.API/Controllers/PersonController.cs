﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Resume.API.Models;
using Resume.API.Services;
using AutoMapper;

namespace Resume.API.Controllers
{
    [Route("api/person")]
    public class PersonController : Controller
    {       
        private ILogger<PersonController> _logger;
        private IResumeRepository _resumeRepository;

        public PersonController(ILogger<PersonController> logger,
            IResumeRepository resumeRepository)
        {
            _logger = logger;
            _resumeRepository = resumeRepository;
        }

        [HttpGet("{personId}", Name = "GetPerson")]
        public IActionResult GetPerson(int personId)
        {
            try
            {
                if (!_resumeRepository.PersonExists(personId))
                {
                    _logger.LogInformation($"Person with id {personId} wasn't found when accessing person.");
                    return NotFound();
                }

                var personnalInfo = _resumeRepository.GetResume(personId);

                //var personnalInfoResults = Mapper.Map<IEnumerable<PersonDto>>(personnalInfo);
                var personnalInfoResults = Mapper.Map<PersonDto>(personnalInfo);
                return Ok(personnalInfoResults);

            }
            catch (Exception ex)
            {
                _logger.LogCritical($"Exception while getting person with id {personId}.", ex);
                return StatusCode(500, "A problem happened while handling your request.");
            }
        }

        [HttpPost]
        public IActionResult CreatePersonalInfo([FromBody] PersonForCreationDto person)
        {
            if (person == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var finalPerson = Mapper.Map<Entities.Person>(person);

            _resumeRepository.AddPerson(finalPerson);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            var createdPersonToReturn = Mapper.Map<Models.PersonDto>(finalPerson);

            return CreatedAtRoute("GetPerson", new
            { personId = finalPerson.PersonId }, createdPersonToReturn);
        }

        [HttpPut("{personId}")]
        public IActionResult UpdatePersonalInfo(int personId, 
            [FromBody] PersonForUpdateDto person)
        {
            if (person == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var personEntity = _resumeRepository.GetPersonnalInfo(personId);
            if (personEntity == null)
            {
                return NotFound();
            }

            Mapper.Map(person, personEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpPatch("{personId}")]
        public IActionResult PartiallyUpdatePersonalInfo(int personId, 
        [FromBody] JsonPatchDocument<PersonForUpdateDto> patchDoc)
        {
            if (patchDoc == null)
            {
                return BadRequest();
            }

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }


            var personEntity = _resumeRepository.GetPersonnalInfo(personId);

            if (personEntity == null)
            {
                return NotFound();
            }
            var peronToPatch = Mapper.Map<PersonForUpdateDto>(personEntity);

            patchDoc.ApplyTo(peronToPatch, ModelState);

            TryValidateModel(peronToPatch);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Mapper.Map(peronToPatch, personEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            return NoContent();
        }

        [HttpDelete("{personId}")]
        public IActionResult DeletePersonalInfo(int personId)
        {

            if (!_resumeRepository.PersonExists(personId))
            {
                return NotFound();
            }

            var personEntity = _resumeRepository.GetPersonnalInfo(personId);
            if (personEntity == null)
            {
                return NotFound();
            }

            _resumeRepository.DeletePerson(personEntity);

            if (!_resumeRepository.Save())
            {
                return StatusCode(500, "A problem happened while handling your request.");
            }

            //_mailService.Send("point of interest deleted.",
            //    $"point of interest {pointOfInterestEntity.Name} with id {pointOfInterestEntity.Id} was deleted.");

            return NoContent();
        }
    }
}
