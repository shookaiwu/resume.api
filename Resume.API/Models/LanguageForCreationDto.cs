﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Resume.API.Models
{
    public class LanguageForCreationDto
    {
        [Required(ErrorMessage = "Please enter Language Name")]
        public string LanguageName { get; set; }

        [Required(ErrorMessage = "Please select Proficiency")]
        public string Proficiency { get; set; }
    }
}
