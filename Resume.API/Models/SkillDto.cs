﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Resume.API.Models
{
    public class SkillDto
    {
        [Required(ErrorMessage="Please enter your skill name")]
        public string SkillName { get; set; }
    }
}