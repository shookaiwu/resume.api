﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Resume.API.Models
{
    public class SkillForCreationDto
    {
        [Required(ErrorMessage = "Please enter your skill name")]
        public string SkillName { get; set; }
    }
}
