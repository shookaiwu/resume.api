﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Resume.API.Models
{
    public class PersonDto
    {
        public int PersonId { get; set; }

        [Required(ErrorMessage = "Please Your First Name ")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Your Last Name ")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please Your Date Of Birth ")]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please Your Nationality ")]
        public string Nationality { get; set; }

        [Required(ErrorMessage = "Select Your Educational Level ")]
        public string EducationalLevel { get; set; }

        [Required(ErrorMessage = "Please Your Address ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Please Your Phone Number ")]
        public string Tel { get; set; }

        [Required(ErrorMessage = "Please Your Email Address ")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please Your Summary")]
        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }

        [Required(ErrorMessage = "Please Your LinekedIn Profile")]
        [DataType(DataType.Url)]
        public string LinkedInProfile { get; set; }

        [Required(ErrorMessage = "Please Your Facebook Profile")]
        [DataType(DataType.Url)]
        public string FaceBookProfile { get; set; }

        [Required(ErrorMessage = "Please Your C# Corner Profile")]
        [DataType(DataType.Url)]
        public string C_CornerProfile { get; set; }

        [Required(ErrorMessage = "Please Your Twitter Profile")]
        [DataType(DataType.Url)]
        public string TwitterProfile { get; set; }
        public byte[] Profile { get; set; }

        public List<SelectListItem> ListNationality { get; set; }
        public List<SelectListItem> ListEducationalLevel { get; set; }
    }
}