﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Resume.API.Models
{
    public class CertificationForCreationDto
    {
        [Required(ErrorMessage = "Please Your Certification name")]
        public string CertificationName { get; set; }

        [Required(ErrorMessage = "Please enter Certification Authority")]
        public string CertificationAuthority { get; set; }

        [Required(ErrorMessage = "Please select Certification Level")]
        public string LevelCertification { get; set; }

        [Required(ErrorMessage = "Please select achievement date")]
        public Nullable<System.DateTime> FromYear { get; set; }
    }
}
