﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog.Extensions.Logging;
using NLog.Web;
using Resume.API.Entities;
using Resume.API.Services;

namespace Resume.API
{
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddMvcOptions(o => o.OutputFormatters.Add(
                    new XmlDataContractSerializerOutputFormatter()));

            var connectionString = Startup.Configuration["connectionStrings:resumeDBConnectionString"];
            services.AddDbContext<ResumeContext>(o => o.UseSqlServer(connectionString));

            services.AddScoped<IResumeRepository, ResumeRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            ResumeContext resumeContext)
        {
            loggerFactory.AddConsole();

            loggerFactory.AddDebug();
            env.ConfigureNLog("nlog.config");

            loggerFactory.AddNLog();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }

            //resumeContext.EnsureSeedDataForContext();

            app.UseStatusCodePages();

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Person, Models.PersonDto>();
                cfg.CreateMap<Entities.Certification, Models.CertificationDto>();
                cfg.CreateMap<Entities.Skill, Models.SkillDto>();
                cfg.CreateMap<Entities.Education, Models.EducationDto>();
                cfg.CreateMap<Entities.Language, Models.LanguageDto>();
                cfg.CreateMap<Entities.WorkExperience, Models.WorkExperienceDto>();
                cfg.CreateMap<Models.CertificationForCreationDto, Entities.Certification>();
                cfg.CreateMap<Models.CertificationForUpdateDto, Entities.Certification>();
                cfg.CreateMap<Models.PersonForCreationDto, Entities.Person>();
                cfg.CreateMap<Models.SkillForCreationDto, Entities.Skill>();
                cfg.CreateMap<Models.LanguageForCreationDto, Entities.Language>();
                cfg.CreateMap<Models.PersonForUpdateDto, Entities.Person>();
                cfg.CreateMap<Models.EducationDto, Entities.Education>();
                cfg.CreateMap<Models.WorkExperienceForCreationDto, Entities.WorkExperience>();
            });

            app.UseMvc();
        }
    }
}
