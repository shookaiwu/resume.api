﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.EntityFrameworkCore;
using Resume.API.Entities;
using Microsoft.AspNetCore.Http;

namespace Resume.API.Services
{
   public interface IResumeRepository
    {
        bool PersonExists(int personId);
        void AddPerson(Person person);
        bool AddPersonnalInformation(Person person, IFormFile file);
        string AddOrUpdateEducation(Education education, int personId);
        int GetIdPerson(string firstName, string lastName);
        string AddOrUpdateExperience(WorkExperience workExperience, int personId);
        bool AddSkill(Skill skill, int personId);
        bool AddCertification(Certification certification, int personId);
        bool AddLanguage(Language language, int personId);
        Person GetPersonnalInfo(int personId);
        IEnumerable<Education> GetEducationsById(int personId);
        Education GetEducationById(int personId, int educationId);
        Person GetResume(int personId);
        IEnumerable<Person> GetAllResumes();
        IEnumerable<WorkExperience> GetWorkExperienceById(int personId);
        WorkExperience GetWorkExperienceById(int personId, int workExperienceId);
        IEnumerable<Skill> GetSkillsById(int personId);
        Skill GetSkillById(int personId, int skillId);
        IEnumerable<Certification> GetCertificationsById(int personId);
        Certification GetCertificationById(int personId, int certificationId);
        IEnumerable<Language> GetLanguagesById(int personId);
        Language GetLanguageById(int personId, int languageId);
        void DeleteCertification(Certification certification);
        void DeletePerson(Person person);
        bool Save();

    }
}
