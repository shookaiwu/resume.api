﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Data;
using AutoMapper.QueryableExtensions;
using Resume.API.Models;
using Resume.API.Entities;
using Microsoft.AspNetCore.Http;

namespace Resume.API.Services
{
    public class ResumeRepository : IResumeRepository
    {
        private ResumeContext _context;

        public ResumeRepository(ResumeContext context)
        {
            _context = context;
        }

        public bool PersonExists(int personId)
        {
            return _context.People.Any(p => p.PersonId == personId);
        }

        public void AddPerson(Person person)
        {
            //Person personEntity = person;
            //personEntity.Add(person);
            _context.People.Add(person);
            _context.SaveChanges();
        }

        public bool AddCertification(Certification certification, int personId)
        {
            int countRecords = 0;
            Person personEntity = _context.People.Find(personId);

            if (personEntity != null && certification != null)
            {
                personEntity.Certifications.Add(certification);
                countRecords = _context.SaveChanges();
            }

            return countRecords > 0 ? true : false;
          
        }

        public bool AddLanguage(Language language, int personId)
        {
            int countRecords = 0;
            Person personEntity = _context.People.Find(personId);
            //Person personEntity = GetResume(personId);
            if (personEntity != null && language != null)
            {
                personEntity.Languages.Add(language);
                countRecords = _context.SaveChanges();
            }

            return countRecords > 0 ? true : false;
        }

        public string AddOrUpdateEducation(Education education, int personId)
        {
            string msg = string.Empty;

            Person personEntity = _context.People.Find(personId);

            if(personEntity != null)
            {
                if (education.EducationId > 0)
                {
                    //we will update education entity
                    _context.Entry(education).State = EntityState.Modified;
                    _context.SaveChanges();

                    msg = "Education entity has been updated successfully";
                }
                else
                {
                    // we will add new education entity
                    personEntity.Educations.Add(education);
                    _context.SaveChanges();

                    msg = "Education entity has been Added successfully";
                }
            }

            return msg;
        }

        public string AddOrUpdateExperience(WorkExperience workExperience, int personId)
        {
            string msg = string.Empty;

            Person personEntity = _context.People.Find(personId);

            if (personEntity != null)
            {
                if (workExperience.WorkExperienceId > 0)
                {
                    //we will update work experience entity
                    _context.Entry(workExperience).State = EntityState.Modified;
                    _context.SaveChanges();

                    msg = "Work Experience entity has been updated successfully";
                }
                else
                {
                    // we will add new work experience entity
                    personEntity.WorkExperiences.Add(workExperience);
                    _context.SaveChanges();

                    msg = "Work Experience entity has been Added successfully";
                }
            }

            return msg;
        }

        public bool AddPersonnalInformation(Person person, IFormFile file)
        {
            //try
            //{
                int nbRecords = 0;

                if (person != null)
                {
                    if (file != null)
                    {
                        person.Profile = ConvertToBytes(file);
                    }

                    _context.People.Add(person);
                    nbRecords = _context.SaveChanges();
                }

                return nbRecords > 0 ? true : false;
            //}
            //catch (DbEntityValidationException dbEx)
            //{

            //    Exception raise = dbEx;
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            string message = string.Format("{0}:{1}",
            //                validationErrors.Entry.Entity.ToString(),
            //                validationError.ErrorMessage);
            //            // raise a new exception nesting
            //            // the current instance as InnerException
            //            raise = new InvalidOperationException(message, raise);
            //        }
            //    }
            //    throw raise;
            //}
            
        }

        public bool AddSkill(Skill skill, int personId)
        {
            int countRecords = 0;
            Person personEntity = _context.People.Find(personId);

            if(personEntity != null && skill != null)
            {
                personEntity.Skills.Add(skill);
                countRecords = _context.SaveChanges();
            }

            return countRecords > 0 ? true : false;

        }

        public IEnumerable<Certification> GetCertificationsById(int personId)
        {
            return _context.Certifications.Where(p => p.PersonId == personId).ToList();
        }

        public Certification GetCertificationById(int personId, int certificationId)
        {
            return _context.Certifications
                .Where(p => p.PersonId == personId && p.CertificationId == certificationId).FirstOrDefault();
        }

        public IEnumerable<Education> GetEducationsById(int personId)
        {
            return _context.Educations.Where(e => e.EducationId == personId).ToList();
        }

        public Education GetEducationById(int personId, int educationId)
        {
            return _context.Educations
                .Where(p => p.PersonId == personId && p.EducationId == educationId).FirstOrDefault();
        }

        public int GetIdPerson(string firstName, string lastName)
        {
            int idSelected = _context.People.Where(p => p.FirstName.ToLower().Equals(firstName.ToLower()))
                                              .Where(p => p.LastName.ToLower().Equals(lastName.ToLower()))
                                              .Select(p => p.PersonId).FirstOrDefault();

            return idSelected;
        }

        public IEnumerable<Language> GetLanguagesById(int personId)
        {
            var languageList = _context.Languages.Where(w => w.LanguageId == personId);
            return languageList;
        }
        public Language GetLanguageById(int personId, int languageId)
        {
            return _context.Languages
                .Where(p => p.PersonId == personId && p.LanguageId == languageId).FirstOrDefault();
        }

        public Person GetPersonnalInfo(int personId)
        {
            return _context.People.Find(personId);

        }

        public IEnumerable<Person> GetAllResumes()
        {
            return _context.People;
        }

        public Person GetResume(int personId)
        {
            return _context.People
                    .Include(c => c.Certifications)
                    .Include(c => c.Educations)
                    .Include(c => c.Languages)
                    .Include(c => c.Skills)
                    .Include(c => c.WorkExperiences)
                    .Where(c => c.PersonId == personId).FirstOrDefault();

        }

        public IEnumerable<Skill> GetSkillsById(int personId)
        {
            return _context.Skills.Where(w => w.PersonId == personId).ToList();
        }

        public Skill GetSkillById(int personId, int skillId)
        {
            return _context.Skills
                .Where(p => p.PersonId == personId && p.SkillId == skillId).FirstOrDefault();
        }

        public IEnumerable<WorkExperience> GetWorkExperienceById(int personId)
        {
            var workExperienceList = _context.WorkExperiences.Where(w => w.PersonId == personId).ToList();
            return workExperienceList;
        }

        public WorkExperience GetWorkExperienceById(int personId, int workExperienceId)
        {
            return _context.WorkExperiences
                .Where(p => p.PersonId == personId && p.WorkExperienceId == workExperienceId).FirstOrDefault();
        }

        private byte[] ConvertToBytes(IFormFile image)
        {
            //byte[] imageBytes = null;
            //BinaryReader reader = new BinaryReader(image.InputStream);
            //imageBytes = reader.ReadBytes((int)image.ContentLength);
            //return imageBytes;
            Stream stream = image.OpenReadStream();
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        public void DeleteCertification(Certification certification)
        {
            _context.Certifications.Remove(certification);
        }

        public void DeletePerson(Person person)
        {
            _context.People.Remove(person);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

    }
}