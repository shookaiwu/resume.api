﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using Resume.API.Entities;
using System;

namespace Resume.API.Migrations
{
    [DbContext(typeof(ResumeContext))]
    [Migration("20180410033751_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.2-rtm-10011")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Resume.API.Entities.Certification", b =>
                {
                    b.Property<int>("CertificationId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CertificationAuthority")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("CertificationName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("FromYear");

                    b.Property<string>("LevelCertification")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("PersonId");

                    b.HasKey("CertificationId");

                    b.HasIndex("PersonId");

                    b.ToTable("Certifications");
                });

            modelBuilder.Entity("Resume.API.Entities.Education", b =>
                {
                    b.Property<int>("EducationId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Degree")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("FromYear");

                    b.Property<string>("InstituteUniversity")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("PersonId");

                    b.Property<string>("TitleOfDiploma")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("ToYear");

                    b.HasKey("EducationId");

                    b.HasIndex("PersonId");

                    b.ToTable("Educations");
                });

            modelBuilder.Entity("Resume.API.Entities.Language", b =>
                {
                    b.Property<int>("LanguageId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LanguageName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("PersonId");

                    b.Property<string>("Proficiency")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("LanguageId");

                    b.HasIndex("PersonId");

                    b.ToTable("Languages");
                });

            modelBuilder.Entity("Resume.API.Entities.Person", b =>
                {
                    b.Property<int>("PersonId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("C_CornerProfile")
                        .HasMaxLength(50);

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<string>("EducationalLevel")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FacebookProfile")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LinkedInProfile")
                        .IsRequired();

                    b.Property<string>("Nationality");

                    b.Property<byte[]>("Profile")
                        .HasMaxLength(500);

                    b.Property<string>("Summary")
                        .HasMaxLength(500);

                    b.Property<string>("Tel")
                        .IsRequired();

                    b.Property<string>("TwitterProfile")
                        .HasMaxLength(50);

                    b.HasKey("PersonId");

                    b.ToTable("People");
                });

            modelBuilder.Entity("Resume.API.Entities.Skill", b =>
                {
                    b.Property<int>("SkillId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("PersonId");

                    b.Property<string>("SkillName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("SkillId");

                    b.HasIndex("PersonId");

                    b.ToTable("Skills");
                });

            modelBuilder.Entity("Resume.API.Entities.WorkExperience", b =>
                {
                    b.Property<int>("WorkExperienceId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Company")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(300);

                    b.Property<DateTime>("FromYear");

                    b.Property<int>("PersonId");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("ToYear");

                    b.HasKey("WorkExperienceId");

                    b.HasIndex("PersonId");

                    b.ToTable("WorkExperiences");
                });

            modelBuilder.Entity("Resume.API.Entities.Certification", b =>
                {
                    b.HasOne("Resume.API.Entities.Person", "Person")
                        .WithMany("Certifications")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Resume.API.Entities.Education", b =>
                {
                    b.HasOne("Resume.API.Entities.Person", "Person")
                        .WithMany("Educations")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Resume.API.Entities.Language", b =>
                {
                    b.HasOne("Resume.API.Entities.Person", "Person")
                        .WithMany("Languages")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Resume.API.Entities.Skill", b =>
                {
                    b.HasOne("Resume.API.Entities.Person", "Person")
                        .WithMany("Skills")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Resume.API.Entities.WorkExperience", b =>
                {
                    b.HasOne("Resume.API.Entities.Person", "Person")
                        .WithMany("WorkExperiences")
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
