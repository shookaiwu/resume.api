﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Resume.API.Entities
{
    public class Certification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CertificationId { get; set; }
        [Required]
        [MaxLength(50)]
        public string CertificationName { get; set; }
        [Required]
        [MaxLength(50)]
        public string CertificationAuthority { get; set; }
        [Required]
        [MaxLength(50)]
        public string LevelCertification { get; set; }
        [Required]
        public DateTime FromYear { get; set; }

        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        public int PersonId { get; set; }

    }
}
