﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Resume.API.Entities
{
    public class Person
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PersonId { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        public string Nationality { get; set; }
        [Required]
        public string EducationalLevel { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Tel { get; set; }
        [Required]
        public string Email { get; set; }
        [MaxLength(500)]
        public string Summary { get; set; }
        [Required(ErrorMessage = "Please Your LinekedIn Profile")]
        [DataType(DataType.Url)]
        public string LinkedInProfile { get; set; }
        [MaxLength(50)]
        public string FacebookProfile { get; set; }
        [MaxLength(50)]
        public string C_CornerProfile { get; set; }
        [MaxLength(50)]
        public string TwitterProfile { get; set; }
        [MaxLength(500)]
        public byte[] Profile { get; set; }

        public ICollection<Certification> Certifications { get; set; }
       = new List<Certification>();

        public ICollection<Education> Educations { get; set; }
        = new List<Education>();

        public ICollection<Language> Languages { get; set; }
        = new List<Language>();

        public ICollection<Skill> Skills { get; set; }
        = new List<Skill>();

        public ICollection<WorkExperience> WorkExperiences { get; set; }
        = new List<WorkExperience>();
    }
}
