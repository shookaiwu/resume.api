﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Resume.API.Entities
{
    public class Education
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EducationId { get; set; }
        [Required]
        [MaxLength(50)]
        public string InstituteUniversity { get; set; }
        [Required]
        [MaxLength(50)]
        public string TitleOfDiploma { get; set; }
        [Required]
        [MaxLength(50)]
        public string Degree { get; set; }
        [Required]
        public DateTime FromYear { get; set; }
        [Required]
        public DateTime ToYear { get; set; }
        [Required]
        [MaxLength(50)]
        public string City { get; set; }
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        public int PersonId { get; set; }
    }
}
